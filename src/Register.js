import React, { useState } from 'react';
import axios from 'axios'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter,Form,Input,Label,FormGroup,Row,Col } from 'reactstrap';
import Login from './Login';


function Register(args) {
  const [modal, setModal] = useState(false);
const [formdata, setFormdata] = useState({
    firstname:'',
    lastname:'',
    email:'',
    password:'',
    address:'',
    state:'',
    city:'',
    zip:''

});
const handleInput = (e) =>{
    const {name, value} = e.target
    setFormdata({
        ...formdata,
        [name]:value
    })
}
console.log(formdata)
  const toggle = () => setModal(!modal);
  /*const toggle = () =>{
    e.preventDefault();
    setModal(!modal);
  }*/
  const handleSubmit = async (e) =>{
    e.preventDefault();
    try{
      let response = await axios.post(`http://localhost:4000/register`,formdata)
      console.log(response)
  
    }catch (err){
      throw err
    }
   }
  return (
    <div>
      <Button color="danger" onClick={toggle}>
        Register Me
      </Button>
      <Modal isOpen={modal} toggle={toggle} {...args}>
        <ModalHeader toggle={toggle}>Registration Form</ModalHeader>
        <ModalBody>

<Form onSubmit={handleSubmit}>
  <Row>
    <Col md={6}>
      <FormGroup>
        <Label for="exampleFirstname">
        Firstname
        </Label>
        <Input
          id="exampleFirstname"
          name="firstname"
          placeholder="firstname"
          type="Firstname"
          value={formdata.firstname}
          onChange={handleInput}
          required
        />
      </FormGroup>
    </Col>
    <Col md={6}>
      <FormGroup>
        <Label for="exampleLastname">
        Lastname
        </Label>
        <Input
          id="exampleLastname"
          name="lastname"
          placeholder="lastname"
          type="lastname"
          value={formdata.lastname}
          onChange={handleInput}
          required
        />
      </FormGroup>
    </Col>
  </Row>
  <Row>
    <Col md={6}>
      <FormGroup>
        <Label for="exampleEmail">
          Email
        </Label>
        <Input
          id="exampleEmail"
          name="email"
          placeholder="with a placeholder"
          type="email"
          value={formdata.email}
          onChange={handleInput}
          required
        />
      </FormGroup>
    </Col>
    <Col md={6}>
      <FormGroup>
        <Label for="examplePassword">
          Password
        </Label>
        <Input
          id="examplePassword"
          name="password"
          placeholder="password placeholder"
          type="password"
          value={formdata.password}
          onChange={handleInput}
          required
        />
      </FormGroup>
    </Col>
    </Row>
    <Row>
    <FormGroup>
    <Label for="exampleAddress">
      Address
    </Label>
    <Input
      id="exampleAddress"
      name="address"
      placeholder="6-51/A,..."
      value={formdata.address}
      onChange={handleInput}
      required
    />
  </FormGroup>
  <FormGroup>
    <Label for="exampleCity">
      City
    </Label>
    <Input
      id="exampleCity"
      name="city"
      placeholder="Apartment,etc"
      value={formdata.city}
      onChange={handleInput}
      required
    />
  </FormGroup>
  </Row>
  <Row>
  <Col md={6}>
      <FormGroup>
        <Label for="exampleState">
          State
        </Label>
        <Input
          id="exampleState"
          name="state"
          value={formdata.state}
          onChange={handleInput}
          required
        />
      </FormGroup>
    </Col>
    <Col md={6}>
      <FormGroup>
        <Label for="exampleZip">
          Zip
        </Label>
        <Input
          id="exampleZip"
          name="zip"
          value={formdata.zip}
          onChange={handleInput}required
        />
      </FormGroup>
    </Col>
  </Row>
  <FormGroup check>
    <Input
      id="exampleCheck"
      name="check"
      type="checkbox"
    />
    <Label
      check
      for="exampleCheck"
    >
      Check me out
    </Label>
  </FormGroup>
    <Button type='Sign in'onClick={toggle} >
      Sign in
    </Button>
  </Form>
        </ModalBody>
        <ModalFooter>
          <Button color="primary" onClick={toggle}>
            Do Something
          </Button>{' '}
          <Button color="secondary" onClick={toggle}>
            Cancel
          </Button>
         < Login/>
        </ModalFooter>
      </Modal>
   
    </div>
  );
}

export default Register;