import React, { useState } from 'react';
import axios from 'axios';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter,Form,Input,Label,FormGroup } from 'reactstrap';

// import {useHistory} from 'react-router-dom' 

function Login(args) {
  // const [isAdmin, setIsAdmin] = useState(false);
 const [modal, setModal] = useState(false);
 const [formdata,setFormdata] = useState({
   email:'',
   password:''
 })
 //const history = useHistory();

 const handleInput = (e) =>{
   const {name,value} =e.target
  
   setFormdata({
     ...formdata,
     [name]:value
       })
     
 }
 console.log(formdata)

 const toggle = (e) => {
   e.preventDefault();
   setModal(!modal)
 };
const handleSubmit = async (e) =>{
   e.preventDefault();
   try{
     let response = await axios.get(`http://localhost:4000/login/${formdata.email}/${formdata.password}`)
     console.log(response)
    //  if (formdata.email === 'admin') {
    //   setIsAdmin(true); // Set isAdmin to true if the email is 'admin'
    // }
    //  if (formdata.email === 'admin') { // Check if the email is 'admin'
    //   history.push('/Admin'); // Redirect to the admin page route
    // } else {
    //   alert('Login successful');
    // }
   }catch (err){
     throw err
   }
    alert("login successfuly")
  }
return (
   <div>
     <Button color="danger" onClick={toggle} >
      login
     </Button>
     
     <Modal isOpen={modal} toggle={toggle} {...args}>
       <ModalHeader toggle={toggle}>Modal title</ModalHeader>
       <ModalBody>
       <>
  <Form onSubmit={handleSubmit}>
   <FormGroup floating>
     <Input
       id="exampleEmail"
       name="email"
       placeholder="Email"
       type="email"
       value = {formdata.email}
       onChange={handleInput} 
       required    


     />
     <Label for="exampleEmail">
       Email
     </Label>
   </FormGroup>
    
   <FormGroup floating>
     <Input
       id="examplePassword"
       name="password"
       placeholder="Password"
       type="password"
       value= {formdata.password}
       onChange={handleInput}
       required
     
     />
     <Label for="examplePassword">
       Password
     </Label>
   </FormGroup>
 
   <Button type='submit' href="Home1">
     Submit
   </Button>
 </Form>
</>
        </ModalBody>
       <ModalFooter>
         <Button color="primary" onClick={toggle}>
           Do Something
         </Button>
         <Button color="secondary" onClick={toggle}>
           Cancel
         </Button>
     
       </ModalFooter>
     </Modal>
     {/*  {isAdmin && (
        <Button color="primary" onClick={() => console.log('Admin button clicked')}>
          Admin
        </Button> 
      )} */}
   </div>
 );
}


export default Login;










