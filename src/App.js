
import './App.css';
//import Aboutus from './Components/Aboutus';
import Home from './Components/Home';
import Head from './Head';
import Makeup from './Components/Makeup';
import Skincare from './Components/Skincare';
import Allservices from './Components/Allservices';



import { BrowserRouter, Routes, Route } from 'react-router-dom'
import Aboutus from './Components/Aboutus';
import Login from './Login';
import Admin from './Components/Admin';
import Home1 from './Components/Home1';

//import ClassComponents from './ClassComponents';
//import FunctionalComponent from './FunctionalComponent';
//import MainPage from './MainPage';
//import Assignment from './Assignment';
//import Customtable from './Customtable';
//import Login from './Login';

// import {Navbar,Nav,NavDropdown} from'react-bootstrap'

function App() {
  return (
    <div>
      <Head />
      <BrowserRouter>

        <Routes>
          {/* <Route path="/" element={<Home />} /> */}
          <Route path="/" element={<Home1 />} />
          <Route path='Home1' element={<Home1 />} />
          {/* <Route path='/Home' element={<Home />}/> */}
          <Route path="/Makeup" element={<Makeup />} />
          <Route path="/Skincare" element={<Skincare />} />
          <Route path="/Aboutus" element={<Aboutus />} />
          <Route path="/Allservices" element={<Allservices />} />
          <Route path="/Admin" element={<Admin/>} />
       
          
        </Routes>

      </BrowserRouter>
    </div>
  )
}

export default App;
