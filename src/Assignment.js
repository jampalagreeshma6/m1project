import React from 'react'
import {Card,CardImg,CardImgOverlay,CardTitle,CardText,CardBody,CardSubtitle,Button} from 'reactstrap'
import Login from './Login'
import Register from './Register'

function Assignment() {
  return (
   <div>

     <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
       <form class="d-flex" style={{width:500,marginTop:20}}>
       <input class="form-control me-sm-2" type="search" placeholder="Search"></input>
      <button class="btn btn-dark my-2 my-sm-0" type="submit">Search</button>
   </form>
     <Login />
     <Register />
</nav>
    
     
    
    
  <div>
  <Card inverse>
    <CardImg
      alt="Card image cap"
      src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR3Ji658LbLQ21tTJg1W1CQkzZn7tgtdr5FwA&usqp=CAU"
      style={{
        backgroundColor:'light pink',
        height: 400
      }} 
      width="100%"
    />
    <CardImgOverlay>
      <CardTitle tag="h5">
        HUNGRY?
      </CardTitle>
      <CardText>
        order food from favourite restaurents near you.
      </CardText>
      <CardText>
        <small className="text-muted">
         <h3>POPULAR CITIES IN INDIA</h3>
          Ahmedabad Bangalore Chennai Delhi Gurgoan Hyderabad Kolkata Mumbai Pune & more
        </small><br></br>
          
       </CardText>
    </CardImgOverlay><br></br>
  </Card></div>

 <br></br>
  <div class="row ">
      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 ">
      <Card
  style={{
    width: '18rem'
  }}
>
  <img
    alt="Sample"
    src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR3Ji658LbLQ21tTJg1W1CQkzZn7tgtdr5FwA&usqp=CAU"
  />
  <CardBody>
    <CardTitle tag="h5">
    GREESHMA restaurent
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6"
    >
   </CardSubtitle>
    <CardText>
    All types of food is available in my restaurent.
    charminar,kukatpally,Gachibowli
    </CardText>
    <Button style={{width:250,backgroundColor:'blueviolet'}}>
    search for items
    </Button>
  </CardBody>
</Card>
  </div> 

      <div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 ">
      <Card
  style={{
    width: '18rem'
  }}
>
  <img
    alt="Sample"
    src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT3dkYnVYepMMKrn3yF2pbgtm1LugZ9uwWDlA&usqp=CAU"
  />
  <CardBody>
    <CardTitle tag="h5">
    KOMALI restaurent
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6"
    >
   </CardSubtitle>
    <CardText>
    All types of food is available in my restaurent.
    charminar,kukatpally,Gachibowli,SR nagar
   </CardText>
    <Button style={{width:250,backgroundColor:'blueviolet'}}>
      search for items
    </Button>
  </CardBody>
</Card>
</div>
<div class="col-xl-4 col-lg-4 col-md-4 col-sm-4 ">
      <Card
  style={{
    width: '18rem'
  }}
>
  <img
    alt="Sample"
    src="https://economictimes.indiatimes.com/thumb/msid-89885004,width-1200,height-900,resizemode-4,imgsize-137412/healthy-food_think.jpg?from=mdr"
  />
  <CardBody>
    <CardTitle tag="h5">
    SPANDANA restaurent
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6"
    >
   </CardSubtitle>
    <CardText>
    All types of food is available in my restaurent.
    charminar,kukatpally,Gachibowli,SR nagar
    </CardText>
    <Button style={{width:250,backgroundColor:'blueviolet'}}>
     search for items
    </Button>
  </CardBody>
</Card>
</div>
  </div> 

  <footer class="w-100 py-4 flex-shrink-0" style={{backgroundColor:'black'}}>
        <div class="container py-4">
            <div class="row gy-4 gx-5">
                <div class="col-lg-4 col-md-6">
                    <h3 class="h1 text-white">Restaurent</h3>
                    <ul class="list-unstyled text-muted">
                        <li><a href="#">About us</a></li>
                        <li><a href="#">Team</a></li>
                        <li><a href="#">Careers</a></li>
                        <li><a href="#">Zomoto Blog</a></li>
                        <li><a href="#">Bug bounty</a></li>
                        <li><a href="#">Zomoto corporate</a></li>
                        <li><a href="#">zomoto instamart</a></li>
                        <li><a href="#">zomoto genie</a></li>
                        <li><a href="#">Zomoto HDFC Bank credit card</a></li>
                        <li><a href="#">ESG</a></li>
                    </ul>
                </div>
                <div class="col-lg-2 col-md-6">
                    <h5 class="text-white mb-3">Quick links</h5>
                    <ul class="list-unstyled text-muted">
                    <li><a href="#">Help & Support</a></li>
                        <li><a href="#">partner with us</a></li>
                        <li><a href="#">Ride with us</a></li>
                    </ul>
                </div>
                <div class="col-lg-2 col-md-6">
                    <h5 class="text-white mb-3">CONTACT </h5>
                    <ul class="list-unstyled text-muted">
                    <li><a href="#">Home</a></li>
                        <li><a href="#">About</a></li>
                        <li><a href="#">Get started</a></li>
                        <li><a href="#">FAQ</a></li>
                        
                    </ul>
                </div>
                <div class="col-lg-4 col-md-6">
                    <h5 class="text-white mb-3">Newsletter</h5>
                    <p class="small text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
                    <form action="#">
                        <div class="input-group mb-3">
                            <input class="form-control" type="text" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="button-addon2"></input>
                            <button class="btn btn-primary" id="button-addon2" type="button"><i class="fas fa-paper-plane"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </footer>


 </div> 
 )
}
 
export default Assignment ;