import React, { useState, useEffect } from 'react';
 import { Table } from 'reactstrap';
import axios from 'axios';



function Customtable() {
    const [data, setData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
     
        let response = await axios.get("http://localhost:4000/fetch");
        console.log(response.data);
        
        setData(response.data);
      } catch (err) {
        throw err
      }
    };

    
    fetchData();
  }, []); 

 /* const [data, setData] = useState([{
    id:101,
    name:'Mahesh',
    batchno:58,
    qualification:"B-tech",
    
  },{
    id:102,
    name:'Sai',
    batchno:58,
    qualification:"B-tech",

  },
  {
    id:103,
    name:'Sunil',
    batchno:58,
    qualification:"B-tech",

  }
])*/
  return (
    <div>
      <Table>
        <thead>
         <tr>
         <th>Id</th>
          <th>Name</th>
          <th>Batch No</th>
          <th>Qualification</th>
         </tr>

        </thead>
        <tbody>
           {data.map((item)=>(
           
             <tr>
            <td>{item.id}</td>
            <td>{item.name}</td>
            <td>{item.batchno}</td>
            <td>{item.qualifiaction}</td>
            <td><button className='btn btn-primary'>Delete</button></td>
            <td><button className='btn btn-primary'>Update</button></td>

           </tr>
            
           

           ))}

          

           
        </tbody>
      </Table>
      
    </div>
  )
}

export default Customtable
