
const { createStore } = require("redux");

// Action types
const ADD_TODO = "ADD_TODO";
const REMOVE_TODO = "REMOVE_TODO";

// Initial state
const initialState = {
  todos: [],
};

// Reducer function
const todoReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TODO:
      return {
        ...state,
        todos: [
          ...state.todos,
          {
            id: action.id,
            text: action.text,
          },
        ],
      };
    case REMOVE_TODO:
      return {
        ...state,
        todos: state.todos.filter((todo) => todo.id !== action.id),
      };
    default:
      return state;
  }
};

// Create the store
const store = createStore(todoReducer);

// Subscribe to store changes
store.subscribe(() => {
  console.log("Current State:", store.getState());
});

// Dispatching actions
const addTodo = (id, text) => ({
  type: ADD_TODO,
  id,
  text,
});

const removeTodo = (id) => ({
  type: REMOVE_TODO,
  id,
});

// Dispatch ADD_TODO action
store.dispatch(addTodo(1, "greeshma"));
store.dispatch(addTodo(2, "sumathi"));

// Dispatch REMOVE_TODO action
store.dispatch(removeTodo(1));
