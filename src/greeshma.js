import React, { Component } from 'react'
import {form,nav} from 'bootstrap'
import { Card,CardBody,CardTitle,CardSubtitle,CardText,Button,CardImg,CardImgOverlay } from 'reactstrap';
import Login from './Login';
import Register from './Register';

export class AssignmentUi extends Component {
  render() {
    return (
    <div>
          
        <nav class="navbar navbar-expand-lg bg-dark" data-bs-theme="dark">
        <div class="container-fluid">
          <a class="navbar-brand" href="#" style={{color:'yellowgreen'}}>ToursForU</a>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarColor01">
            <ul class="navbar-nav me-auto">
              <li class="nav-item">
                <a class="nav-link active" href="#">Home
                  <span class="visually-hidden">(current)</span>
                </a>
              </li>
              {/* <li class="nav-item">
                <a class="nav-link" href="#">Features</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">register</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">login</a>
              </li> */}
              {/* <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-bs-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Dropdown</a>
                <div class="dropdown-menu">
                  <a class="dropdown-item" href="#">Action</a>
                  <a class="dropdown-item" href="#">Another action</a>
                  <a class="dropdown-item" href="#">Something else here</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#">Separated link</a>
                </div>
              </li> */}
            </ul>
            <form class="d-flex" style={{width:1000,marginTop:20}}>
              <input class="form-control me-sm-2" type="search" placeholder="Search"></input>
              <button class="btn btn-dark my-2 my-sm-0" type="submit">Search</button>
            </form>
          </div>
          
        </div>
        <div style={{marginRight:30}}>
  <Register/> 
    
    </div>
    <div style={{marginRight:100}}>  
       <Login />
    </div>
    

      </nav>
      <br></br>
      <div>
  <Card inverse>
    <CardImg
      alt="Card image cap"
      src="https://www.google.com/imgres?imgurl=https%3A%2F%2Fimages.pexels.com%2Fphotos%2F958545%2Fpexels-photo-958545.jpeg%3Fcs%3Dsrgb%26dl%3Dpexels-chan-walrus-958545.jpg%26fm%3Djpg&tbnid=ISQq3Skk607IkM&vet=12ahUKEwjeydXCgdOCAxU37DgGHRwsBfIQMyg4egUIARDxAQ..i&imgrefurl=https%3A%2F%2Fwww.pexels.com%2Fsearch%2Ffood%2F&docid=c1W1wgDZbrCp_M&w=4608&h=2592&q=food%20items%20images&hl=en-GB&ved=2ahUKEwjeydXCgdOCAxU37DgGHRwsBfIQMyg4egUIARDxAQ"
      style={{
        height: 700
      }}
      width="100%"
    />
    <CardImgOverlay>
      <CardTitle tag="h2">
      POPULAR CAIRNS TOURS & ACTIVITIES

      </CardTitle>
      <CardText>
      discover Cairns with one of these self-guided-tours      </CardText>
      <CardText>
        <small className="text-muted">
          
        </small>
      </CardText>
    </CardImgOverlay>
  </Card>
</div>
      <br></br>

  
      <div class="row ">
      <div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 ">
      <Card
  style={{
    width: '18rem'
  }}
>
  <img
    alt="Sample"
    src="https://th.bing.com/th/id/OLC.xP0kntliGwuHbw480x360?&rs=1&pid=ImgDetMain/300/200"
  />
  <CardBody>
    <CardTitle tag="h5">
    Charminar
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6"
    >
      Monument

    </CardSubtitle>
    <CardText>
    Charminar Road, Kotla Alijah, Moghalpura, Hyderabad, Telangana 500002 · ~32.1 km
040 6674 5986
telanganatourism.gov.in
    </CardText>
    <Button style={{width:250,backgroundColor:'blueviolet'}}>
      wikipedia
    </Button>
  </CardBody>
</Card>
</div>
<div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 ">
<Card
  style={{
    width: '18rem'
  }}
>
  <img
    alt="Sample"
    src="https://th.bing.com/th/id/OLC.kgQcviufReq8Sg480x360?&rs=1&pid=ImgDetMain/300/200"
  />
  <CardBody>
    <CardTitle tag="h5">
    Salar Jung Museum
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6"
    >
      History museum
    </CardSubtitle>
    <CardText>
    Salar Jung Rd ,                                 Hyderabad, Telangana 500002 · ~33.2 km
040 2457 6443
salarjungmuseum.in


    </CardText>
    <Button  style={{width:250,backgroundColor:'blueviolet'}}>
      wikipedia
    </Button>
  </CardBody>
</Card>
</div>
<div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 ">
<Card
  style={{
    width: '18rem'
  }}
>
  <img
    alt="Sample"
    src="https://i1.wp.com/azureskyfollows.com/wp-content/uploads/2017/11/215-chowmahalla-palace-hyderabad-telengana-azure-sky-follows-tania-mukherjee-banerjee.jpeg?resize=960%2C640/300/200"
  />
  <CardBody>
    <CardTitle tag="h5">
    Chowmahalla Palace
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6"
    >
      Palace
    </CardSubtitle>
    <CardText>
    20-4-236, Motigalli, Khilwat, Hyderabad, Telangana 500002 · ~31.7 km
040 2452 2032
telanganatourism.gov.in

    </CardText>
    <Button  style={{width:250,backgroundColor:'blueviolet'}}>
      wikipedia
    </Button>
  </CardBody>
</Card>
</div>
<div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 ">

<Card
  style={{
    width: '18rem'
  }}
>
  <img
    alt="Sample"
    src="https://th.bing.com/th/id/OLC.Duyg0goMvIoVFQ480x360?&rs=1&pid=ImgDetMain/300/200"
  />
  <CardBody>
    <CardTitle tag="h5">
    Golconda Fort
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6"
    >
       Fort
    </CardSubtitle>
    <CardText>
    Ibrahim Bagh, Hyderabad, Telangana 500008 · ~35.4 km
040 2351 2401
telanganatourism.gov.in
    </CardText>
    <Button  style={{width:250,backgroundColor:'blueviolet'}}>
      wikipedia
    </Button>
  </CardBody>
</Card>
</div>
<div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 ">

<Card
  style={{
    width: '18rem'
  }}
>
  <img
    alt="Sample"
    src="https://www.bing.com/th?id=OLC.pp2aAVH7QzhnSA480x360&w=249&h=140&c=8&rs=1&qlt=90&dpr=1.3&pid=3.1&rm=2/300/200"
  />
  <CardBody>
    <CardTitle tag="h5">
    Hussain Sagar Lake
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6"
    >
    Train station
    </CardSubtitle>
    <CardText>
    2 Km From Hyderabad Center, Hyderabad, Telangana 500029 · ~39.1 km
088959 60957
ovacabshyderabad.com

    </CardText>
    <Button  style={{width:250,backgroundColor:'blueviolet'}}>
      wikipedia
    </Button>
  </CardBody>
</Card>
</div>
<div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 ">

<Card
  style={{
    width: '18rem'
  }}
>
  <img
    alt="Sample"
    src="https://www.bing.com/th?id=OLC.lQUuVFqe6pj6UA480x360&w=207&h=140&c=8&rs=1&qlt=90&dpr=1.3&pid=3.1&rm=2/300/200"
  />
  <CardBody>
    <CardTitle tag="h5">
    Ocean Park
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6"
    >
    Bus station

    </CardSubtitle>
    <CardText>
    Shankerpally Road, Near Cbit College, Hyderabad, Telangana 500075 · ~38.5 km
040 2419 3235
facebook.com
    </CardText>
    <Button  style={{width:250,backgroundColor:'blueviolet'}}>
      wikipedia
    </Button>
  </CardBody>
</Card>
</div>
<div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 ">

<Card
  style={{
    width: '18rem'
  }}
>
  <img
    alt="Sample"
    src="https://www.bing.com/th?id=OLC.JLDJLbH2xe75rg480x360&w=209&h=140&c=8&rs=1&qlt=90&dpr=1.3&pid=3.1&rm=2/300/200"
  />
  <CardBody>
    <CardTitle tag="h5">
    Wonderla Amusement Park
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6"
    >
     Amusement park

    </CardSubtitle>
    <CardText>Outer Ring Road, Exit No 13, Hyderabad, Telangana 501510 · ~17 km
091000 63636
wonderla.com
    </CardText>
    <Button  style={{width:250,backgroundColor:'blueviolet'}}>
      wikipedia
    </Button>
  </CardBody>
</Card>
</div>
<div class="col-xl-3 col-lg-3 col-md-3 col-sm-4 ">

<Card
  style={{
    width: '18rem'
  }}
>
  <img
    alt="Sample"
    src="https://www.bing.com/th?id=OLC.J83yOqi+0G0hHg480x360&w=262&h=140&c=8&rs=1&qlt=90&dpr=1.3&pid=3.1&rm=2/300/200"
  />
  <CardBody>
    <CardTitle tag="h5">

Birla Planetarium Science Center
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6"
    >
      Science museum

    </CardSubtitle>
    <CardText>
    Ambedkar Colony, Khairtabad, Hyderabad, Telangana 500004 · ~36.8 km
040 2323 5081
gpbaasri.org

    </CardText>
    <Button  style={{width:250,backgroundColor:'blueviolet'}}>
      wikipedia
    </Button>
  </CardBody>
</Card>
</div>

      </div>
      <br></br>

      
      <footer class="w-100 py-4 flex-shrink-0" style={{backgroundColor:'black'}}>
        <div class="container py-4">
            <div class="row gy-4 gx-5">
                <div class="col-lg-4 col-md-6">
                    <h5 class="h1 text-white">FB.</h5>
                    <p class="small text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
                    <p class="small text-muted mb-0"> & copy; Copyrights. All rights reserved. <a class="text-primary" href="#">Bootstrapious.com</a></p>
                </div>
                <div class="col-lg-2 col-md-6">
                    <h5 class="text-white mb-3">Quick links</h5>
                    <ul class="list-unstyled text-muted">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">About</a></li>
                        <li><a href="#">Get started</a></li>
                        <li><a href="#">FAQ</a></li>
                    </ul>
                </div>
                <div class="col-lg-2 col-md-6">
                    <h5 class="text-white mb-3">Quick links</h5>
                    <ul class="list-unstyled text-muted">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">About</a></li>
                        <li><a href="#">Get started</a></li>
                        <li><a href="#">FAQ</a></li>
                    </ul>
                </div>
                <div class="col-lg-4 col-md-6">
                    <h5 class="text-white mb-3">Newsletter</h5>
                    <p class="small text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
                    <form action="#">
                        <div class="input-group mb-3">
                            <input class="form-control" type="text" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="button-addon2"></input>
                            <button class="btn btn-primary" id="button-addon2" type="button"><i class="fas fa-paper-plane"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </footer>









     
      </div>
      //  </div> 
    )
  }
}

export default AssignmentUi