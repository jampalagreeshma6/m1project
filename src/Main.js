import React from 'react';

function SearchBar() {
  const handleSubmit = (e) => {
    e.preventDefault();
    const searchTerm = e.target.elements.search.value; // Get the value from the input field
    if (searchTerm) {
      // Redirect to the new URL based on the search term
      window.location.href = `/search/${encodeURIComponent(searchTerm)}`;
    }
  };

  return (
    <form className="d-flex" onSubmit={handleSubmit}>
      <img src="http://www.clker.com/cliparts/B/t/2/T/7/O/search-icon-small-16x16.svg.thumb.png" style={{ height: '40px', width: '50px' }} alt="Search Icon" />
      <input className="form-control me-sm-2" type="search" placeholder="Search" name="search" />
      <button className="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
    </form>
  );
}

export default SearchBar;
