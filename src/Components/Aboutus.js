import React from 'react'
import { Card,CardTitle,CardText,CardBody,CardSubtitle,Button } from 'reactstrap';
function Aboutus() {
  return (
    <div>
        <div class ="row">
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 ">
      <Card
  style={{
    width: '40rem'
  }}
>
  <img
    alt="Sample"
    src="https://media.ulta.com/i/ulta/About_ulta_products_3up?w=659"
  />
  <CardBody>
    <CardTitle tag="h5">
     Products
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6"
    >
   </CardSubtitle>
    <CardText>
   All things beauty,all in one place@.over 25,000 products.A mix of high and low.
    A range of shades and looks for all. Running the gamut of makeup,skin,hair,fragrance and
    accessories.Find whatever is it that makes you shine.
    </CardText>
    {/* <Button style={{width:250,backgroundColor:'blueviolet'}}>
     
    </Button> */}
    <a>shop now</a>
  </CardBody>
</Card>
</div>
<div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 ">
      <Card
  style={{
    width: '40rem'
  }}
>
  <img
    alt="Sample"
    src="https://media.ulta.com/i/ulta/About_ulta_the_salon_3up?w=659"
  />
  <CardBody>
    <CardTitle tag="h5">
     The salon at Ultra Beauty
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6"
    >
   </CardSubtitle>
    <CardText>
    With safety always at heart ,our expert hair,brow and skin services are just a for ways we love to connect with our guests-
     and help them look ,feel  and amazing.
    </CardText>
    {/* <Button style={{width:250,backgroundColor:'blueviolet'}}>
     
    </Button> */}
    <a>learn more</a>
  </CardBody>
</Card>
</div>
<p>
<h2>The Possibilities are Beautiful®</h2>
Our goals—Celebrating the role of beauty in our lives. Redefining how beauty is portrayed like in our Beauty & campaign. Supporting trailblazing causes. And driving more meaningful purpose for our brand, our company and our guests. Together, we will change what beauty means to the world.

<h2>Vision</h2>
To be the most loved beauty destination of our guests and the most admired retailer by our Ulta Beauty associates, communities, partners and investors.

<h2>Mission</h2>
Every day, we use the power of beauty to bring to life the possibilities that lie within each of us — inspiring every guest and enabling each associate to build a fulfilling career.

<h2>Values</h2>
We work toward our vision and mission with our values at the heart of everything we do.</p>
    </div>
    <div style={{marginLeft:'250px'}}>
    <img src="https://media.ulta.com/i/ulta/about_us_values?w=840"></img>
    </div>
    </div>
    
  )
}

export default Aboutus