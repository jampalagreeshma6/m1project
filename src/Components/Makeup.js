import React from 'react'
import { Card,CardTitle,CardText,CardBody,CardSubtitle,Button } from 'reactstrap';
function Makeup() {
  return (
    <div style={{marginTop:'80px'}}>
        <h1>Makeup Products</h1>
        <div class="row ">
        <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 ">
      <Card
  style={{
    width: '18rem'
  }}
>
  <img
    alt="Sample"
    src="https://media.ulta.com/i/ulta/2510664?w=300&$ProductCardNeutralBGLight$"
  />
  <CardBody>
    <CardTitle tag="h5">
     Foundation
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6"
    >
   </CardSubtitle>
    <CardText>
   
    </CardText>
    <Button style={{width:250,backgroundColor:'blueviolet'}} href="Buy">
    Buy now
    </Button>
  </CardBody>
</Card>
</div>
<div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 ">
      <Card
  style={{
    width: '18rem'
  }}
>
  <img
    alt="Sample"
    src="https://media.ulta.com/i/ulta/2569041?w=300&$ProductCardNeutralBGLight$"

  />
  <CardBody>
    <CardTitle tag="h5">
    Eyeshadow Palettes
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6"
    >
   </CardSubtitle>
    <CardText>
   
    </CardText>
    <Button style={{width:250,backgroundColor:'blueviolet'}}>
    Buy now
    </Button>
  </CardBody>
</Card>
</div>
<div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 ">
      <Card
  style={{
    width: '18rem'
  }}
>
  <img
    alt="Sample"
    src="https://media.ulta.com/i/ulta/2153395?w=300&$ProductCardNeutralBGLight$"
  />
  <CardBody>
    <CardTitle tag="h5">
     Lipstick
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6"
    >
   </CardSubtitle>
    <CardText>
   
    </CardText>
    <Button style={{width:250,backgroundColor:'blueviolet'}}>
    Buy now
    </Button>
  </CardBody>
</Card>
</div>
<div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 ">
      <Card
  style={{
    width: '18rem'
  }}
>
  <img
    alt="Sample"
    src="https://media.ulta.com/i/ulta/makeup-bags-cases-bubble?w=245&h=245"
  />
  <CardBody>
    <CardTitle tag="h5">
     Makeup bags
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6"
    >
   </CardSubtitle>
    <CardText>
   
    </CardText>
    <Button style={{width:250,backgroundColor:'blueviolet'}}>
    Buy now
    </Button>
  </CardBody>
</Card>
</div>
    </div>
</div>    
  )
}

export default Makeup