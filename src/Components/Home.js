import React from 'react';
import { Card,CardImg,CardImgOverlay,CardTitle,CardText,CardBody,CardSubtitle,Button } from 'reactstrap';
function Home() {
  const handleSubmit = (e) => {
    e.preventDefault();
    const searchTerm = e.target.elements.search.value; // Get the value from the input field
    if (searchTerm) {
      // Redirect to the new URL based on the search term
      window.location.href = `/search/${encodeURIComponent(searchTerm)}`;
    }
  };



  return (
    <div style={{marginTop:'50px'}}>
    <div>
  <Card inverse>
    <CardImg
      alt="Card image cap"
      src="https://media.ulta.com/i/ulta/HP_WK4523_Hero_MemberLove_XL?w=984&$Magenta100BGLight$"
      style={{
        backgroundColor:'light pink',
        height: 400
      }} 
      width="100%"
    />
    <CardImgOverlay>
      <CardTitle tag="h5">
       
      </CardTitle>
      <CardText>
       
      </CardText>
      <CardText>
        <small className="text-muted">
        
        </small><br></br>
          
       </CardText>
    </CardImgOverlay><br></br>
  </Card></div>

 <br></br>
  <div class="row ">
      <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 ">
      <Card
  style={{
    width: '18rem'
  }}
>
  <img
    alt="Sample"
    src="https://media.ulta.com/i/ulta/Haulidays_WK4523_50offLancome?w=410&$Neutral00BGLight$"
  />
  <CardBody>
    <CardTitle tag="h5">
    50% off Lancome
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6"
    >
   </CardSubtitle>
    <CardText>
 
    </CardText>
    <Button style={{width:250,backgroundColor:'blueviolet'}} href="Allservices">
      search for items
    </Button>
  </CardBody>
</Card>
  </div> 

      <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 ">
      <Card
  style={{
    width: '18rem'
  }}
>
  <img
    alt="Sample"
    src="https://media.ulta.com/i/ulta/Haulidays_WK4523_30offHairTools?w=410&$Neutral00BGLight$"
  />
  <CardBody>
    <CardTitle tag="h5">
    30% on HairTools
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6"
    >
   </CardSubtitle>
    <CardText>
  
   </CardText>
    <Button style={{width:250,backgroundColor:'blueviolet'}}>
      search for items
    </Button>
  </CardBody>
</Card>
</div>
<div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 ">
      <Card
  style={{
    width: '18rem'
  }}
>
  <img
    alt="Sample"
    src="https://media.ulta.com/i/ulta/Haulidays_WK4523_WHIM?w=410&$Neutral00BGLight$"
  />
  <CardBody>
    <CardTitle tag="h5">
    40% on Ultra beauty collection
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6"
    >
   </CardSubtitle>
    <CardText>
   
    </CardText>
    <Button style={{width:250,backgroundColor:'blueviolet'}}>
     search for items
    </Button>
  </CardBody>
</Card>
</div>
<div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 ">
      <Card
  style={{
    width: '18rem'
  }}
>
  <img
    alt="Sample"
    src="https://media.ulta.com/i/ulta/Haulidays_WK4523_Morphe?w=410&$Neutral00BGLight$"
  />
  <CardBody>
    <CardTitle tag="h5">
     40% on morphe
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6"
    >
   </CardSubtitle>
    <CardText>
   
    </CardText>
    <Button style={{width:250,backgroundColor:'blueviolet'}}>
     search for items
    </Button>
  </CardBody>
</Card>
</div>
  </div> 
      
</div>
  )
}

export default Home