import React from 'react'
import { Card,CardTitle,CardText,CardBody,CardSubtitle,Button } from 'reactstrap';
function Skincare() {
  return (
    <div style={{marginTop:'80px'}}>
        <h1>Skin Care Products</h1>
        <div class="row ">
        <div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 ">
      <Card
  style={{
    width: '18rem'
  }}
>
  <img
    alt="Sample"
    src="https://media.ulta.com/i/ulta/2532485?w=300&$ProductCardNeutralBGLight$"
  />
  <CardBody>
    <CardTitle tag="h5">
     Face Wash
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6"
    >
   </CardSubtitle>
    <CardText>
   
    </CardText>
    <Button style={{width:250,backgroundColor:'blueviolet'}}>
    Buy now
    </Button>
  </CardBody>
</Card>
</div>
<div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 ">
      <Card
  style={{
    width: '18rem'
  }}
>
  <img
    alt="Sample"
    src="https://media.ulta.com/i/ulta/2538776?w=300&$ProductCardNeutralBGLight$"

  />
  <CardBody>
    <CardTitle tag="h5">
    Face Serum
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6"
    >
   </CardSubtitle>
    <CardText>
   
    </CardText>
    <Button style={{width:250,backgroundColor:'blueviolet'}}>
    Buy now
    </Button>
  </CardBody>
</Card>
</div>
<div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 ">
      <Card
  style={{
    width: '18rem'
  }}
>
  <img
    alt="Sample"
    src="https://media.ulta.com/i/ulta/cleansing-brushes-bubble?w=245&h=245"
  />
  <CardBody>
    <CardTitle tag="h5">
     Skin Care Tools
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6"
    >
   </CardSubtitle>
    <CardText>
   
    </CardText>
    <Button style={{width:250,backgroundColor:'blueviolet'}}>
   Buy now
    </Button>
  </CardBody>
</Card>
</div>
<div class="col-xl-3 col-lg-3 col-md-4 col-sm-6 ">
      <Card
  style={{
    width: '18rem'
  }}
>
  <img
    alt="Sample"
    src="https://media.ulta.com/i/ulta/2589386?w=300&$ProductCardNeutralBGLight$"
  />
  <CardBody>
    <CardTitle tag="h5">
     Sunscreen
    </CardTitle>
    <CardSubtitle
      className="mb-2 text-muted"
      tag="h6"
    >
   </CardSubtitle>
    <CardText>
   
    </CardText>
    <Button style={{width:250,backgroundColor:'blueviolet'}}>
     Buy now
    </Button>
  </CardBody>
</Card>
</div>
    </div>
    </div>
  )
}

export default Skincare