import React from 'react';
import { useEffect,useState } from 'react';
import axios from 'axios'
import Cards from './Cards';
import { Card,CardImg,CardImgOverlay,CardTitle,CardText } from 'reactstrap';

function Home1() {
    const [data,setData] = useState([]); 
  useEffect(() =>{
    const fetchData =async () =>{
      try{
        let response = await axios.get("http://localhost:4000/fetch")
        console.log(response.data)
        setData(response.data)  
      }
      catch(err){
      }
    };
    fetchData()
},[]);
  return (
    
    <div>
      <Card inverse>
    <CardImg
      alt="Card image cap"
      src="https://media.ulta.com/i/ulta/HP_WK4523_Hero_MemberLove_XL?w=984&$Magenta100BGLight$"
      style={{
        backgroundColor:'light pink',
        height: 400
      }} 
      width="100%"
    />
     </Card>
        <h1 className='text-info text-center text-white fs-10'></h1><span><ur/></span>
          <div className='container'>
          <div className='row mt-5'>{data.map((e)=>(
            // <img src={e.Img} alt=""  className='w-25  rounded-5'/>
            <Cards data={e}/>
          ))} 
        </div>  
        </div>
        </div>
  
  )
}

export default Home1