import React, { useState } from 'react'

export default function FunctionalComponent() {
  const [count,setCount]=useState(1); //hook
  const increment =()=>{
    setCount(count+1)
  }
  const decrement =()=>{
    setCount(count-1)
  }
  const reset =()=>{
    setCount(count=1)
  }

  
  return (
    <div>
      <h1>count:{count}</h1>
    <button onClick={increment}>Increment</button>
    <button onClick={decrement}>Decrement</button>
    <button onClick={reset}>reset</button>
    </div>
 )
 }
 
