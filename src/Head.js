import React from 'react'
import {Navbar, Nav,NavDropdown} from'react-bootstrap';

//import Login from './Login';
import Register from './Register';




function Head() {
  const handleSubmit = (e) => {
    e.preventDefault();
    const searchTerm = e.target.elements.search.value; // Get the value from the input field
    if (searchTerm) {
      // Redirect to the new URL based on the search term
      window.location.href = `/search/${encodeURIComponent(searchTerm)}`;
    }
  };
  return (
    <div style={{marginTop:'50px'}}>
        
       <Navbar bg="primary" variant="white" expand="lg" className='fixed-top'>
      <Navbar.Brand href="Home1"><img src='https://cdn.worldvectorlogo.com/logos/ulta-beauty-1.svg' style={{height:'50px', width:'150px'}} /> </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
         
          <NavDropdown title="Shop" id="basic-nav-dropdown"  >
            <NavDropdown.Item href="Makeup">Makeup</NavDropdown.Item>
            <NavDropdown.Item href="Skincare">Skincare</NavDropdown.Item>
            <NavDropdown.Item href="Hair Care">Hair Care</NavDropdown.Item>
            <NavDropdown.Item href="#Fragrance">#Fragrance</NavDropdown.Item>
            <NavDropdown.Item href="#Tools & Brushes">Tools & Brushes</NavDropdown.Item>
            
            <NavDropdown.Divider />
            <NavDropdown.Item href="#all-services">All Services</NavDropdown.Item>
          </NavDropdown>

          <NavDropdown title="Sale" id="basic-nav-dropdown" style={{marginLeft:'20px'}}>
          <NavDropdown.Item href="Featured Deals">Featured Deals</NavDropdown.Item>
          <NavDropdown.Item href="Sale">Sale</NavDropdown.Item>
          <NavDropdown.Item href="Buy More Save More">Buy More Save More</NavDropdown.Item>
          <NavDropdown.Item href="Gifts With Purchase">Gifts With Purchase</NavDropdown.Item>
          <NavDropdown.Item href="Coupons">Coupons</NavDropdown.Item>
          </NavDropdown>
         
          <NavDropdown title="Beauty Services" id="basic-nav-dropdown" style={{marginLeft:'20px'}} >
          <NavDropdown.Item href="Featured Deals">All Beauty Services</NavDropdown.Item>
          <NavDropdown.Item href="Sale">Hair services</NavDropdown.Item>
          <NavDropdown.Item href="Buy More Save More">Waxing & Brows</NavDropdown.Item>
          <NavDropdown.Item href="Gifts With Purchase">Makeup & Lashes</NavDropdown.Item>
          <NavDropdown.Item href="Coupons">Skin Services</NavDropdown.Item>
          <NavDropdown.Item href="Coupons">ear Piercing</NavDropdown.Item>
          
          
          </NavDropdown>
         
        
      
        </Nav>
        <Navbar.Brand  href="AboutUs" style={{marginLeft:'20px'}}><p>AboutUs</p></Navbar.Brand>

        <Register />

      </Navbar.Collapse>

      <form className="d-flex" onSubmit={handleSubmit}>
      <img src="http://www.clker.com/cliparts/B/t/2/T/7/O/search-icon-small-16x16.svg.thumb.png" style={{ height: '40px', width: '50px' }} alt="Search Icon" />
      <input className="form-control me-sm-2" type="search" placeholder="Search" name="search" />
      <button className="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
    </form>
      
       {/* <form class="d-flex">
        <img src='http://www.clker.com/cliparts/B/t/2/T/7/O/search-icon-small-16x16.svg.thumb.png' style={{height:'40px', width:'50px'}} />
                  <input class="form-control me-sm-2" type="search" placeholder="Search" />
                  <button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
                </form>  */}
               </Navbar>
             
    </div>
  )
}

export default Head